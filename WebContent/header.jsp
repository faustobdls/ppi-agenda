<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Contatos UFERSA</title>
		<link rel="stylesheet" href="assets/bulma/bulma.css">
		<link src="assets/css/styles.css">
		<link rel="stylesheet" href="assets/fa/css/font-awesome.css">
	</head>
	<body>
	<section class="hero is-primary is-bold is-medium">
	  <div class="hero-body">
	    <div class="container">
	      <h1 class="title">
	        Contatos - UFERSA
	      </h1>
	      <h2 class="subtitle">
	        Ramais, emails, e outros contatos importantes
	      </h2>
	    </div>
	  </div>
	</section>