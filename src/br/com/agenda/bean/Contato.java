package br.com.agenda.bean;


public class Contato {
	
	private int id;
	private String nome;
	private String email;
	private String telefone;
	private String ramal;
	private String setor;

	public Contato(){}
	
	public Contato(int i, String nome, String email, String telefone, String ramal, String setor){
		this.setId(id);
		this.setNome(nome);
		this.setEmail(email);
		this.setTelefone(telefone);
		this.setRamal(ramal);
		this.setSetor(setor);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	public String getSetor() {
		return setor;
	}
	public void setSetor(String setor) {
		this.setor = setor;
	}
	
}
