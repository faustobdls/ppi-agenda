package br.com.agenda.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;

import br.com.agenda.bean.Contato;

public class ContatoSQLDAO implements ContatoDAO{

	@Override
	public List<Contato> load() {
		List<Contato> list = new ArrayList<Contato>();
		try {
			Statement statement = SqlConnector.getConnection().createStatement();
			ResultSet resultado = statement.executeQuery("select id, nome, email, telefone, ramal, setor from Contato");
			while(resultado.next()){					
				list.add(
						new Contato(
							resultado.getInt("id"), 
							resultado.getString("nome"), 
							resultado.getString("email"), 
							resultado.getString("telefone"),
							resultado.getString("ramal"), 
							resultado.getString("setor")
						)
					);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public void store(List<Contato> list) {
		try {
			for (Contato cont : list)
			{
				PreparedStatement stmt = SqlConnector.getConnection().prepareStatement("INSERT INTO Contato (id, nome, email, telefone, ramal, setor) VALUES (?, ?, ?, ?, ?, ?)");
				stmt.setInt(1, generateId());
				stmt.setString(2, cont.getNome());	
				stmt.setString(3, cont.getEmail());	
				stmt.setString(4, cont.getTelefone());	
				stmt.setString(5, cont.getRamal());
				stmt.setString(6, cont.getSetor());
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();			
		}		
	}

	@Override
	public List<Contato> filter(String filterTxt) {
		List<Contato> list = new ArrayList<Contato>();
		List<Contato> list2 = new ArrayList<Contato>();
		try {			
			Statement statement = SqlConnector.getConnection().createStatement();
			ResultSet resultado = statement.executeQuery("select id, nome, email, telefone, ramal, setor from contato");
			while(resultado.next()){					
				list.add(new Contato(resultado.getInt("id"), resultado.getString("nome"), resultado.getString("email"), resultado.getString("telefone"),
						resultado.getString("ramal"), resultado.getString("setor")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Contato cont : list){
			if (cont.getNome().contains(filterTxt))
				list2.add(cont);
		}
		return list2;
	}

	@Override
	public void delete(int id) {
		try {
			PreparedStatement stmt = SqlConnector.getConnection().prepareStatement("delete from Contato where id = ?");
			stmt.setInt(1, id);				
			stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public int generateId() {
		try {			
			Statement statement = SqlConnector.getConnection().createStatement();
			ResultSet resultado = statement.executeQuery("select MAX(id) as id from contato");
			while(resultado.next()){					
				return resultado.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

}
