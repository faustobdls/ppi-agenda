package br.com.agenda.dao;

import java.util.List;

public interface DAO<T> {
	public List<T> load();
	
	public void store(List<T> list);
	
	public List<T> filter(String filterTxt);
	
	public void delete(int id);
	
	public int generateId();
}
