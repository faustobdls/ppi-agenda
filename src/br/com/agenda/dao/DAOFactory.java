package br.com.agenda.dao;

public class DAOFactory {
	
	private DAOFactory(){}
	
	public static ContatoDAO getContatoDAO(){
		return new ContatoSQLDAO();
	}
}
