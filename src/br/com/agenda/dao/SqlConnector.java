package br.com.agenda.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlConnector {
	public SqlConnector() {}
	public static Connection getConnection() {
		try {			
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/agenda", "", "");
		} catch (SQLException e) {			
			e.printStackTrace();
		}
		return null;
	}

}
